FROM python:3.8.7-slim AS build

ARG APP_DIR=/var/app
ENV PIP_NO_CACHE_DIR=off

RUN pip install poetry

RUN useradd rates --home-dir "$APP_DIR" --create-home
USER rates

COPY pyproject.* poetry.lock ./
RUN poetry install --no-root

COPY rates/ ./rates/
RUN poetry install

COPY bin/ ./bin/
CMD ["poetry", "run", "bin/run.sh"]
