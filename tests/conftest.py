import pytest
from sqlalchemy import create_engine

from rates.settings import get_settings


@pytest.fixture(scope="session")
def settings():
    return get_settings()


@pytest.fixture(scope="session")
def dbengine(settings):
    return create_engine(settings.db_dsn)


@pytest.fixture
def dbconn(dbengine):
    with dbengine.connect() as conn:
        yield conn
