from datetime import date

import pytest
from pydantic import ValidationError

from rates.prices import (
    Price,
    PriceIngestionError,
    get_average_daily_prices,
    ingest_prices,
)

# Querying tests currently depend on the DB dump so as not to spend time on setting up
# the test cases.
# TODO: redo the tests and fixtures to start with a clean db.


@pytest.mark.database
@pytest.mark.parametrize(
    "date_from,date_to,origin,destination,expected_price_data,",
    [
        (
            "2016-01-01",
            "2016-01-02",
            "CNGGZ",
            "EETLL",
            [("2016-01-01", 1155), ("2016-01-02", 1155)],
        ),
        ("2016-01-01", "2016-01-05", "CNGGZ", "CNGGZ", []),
        ("2016-01-01", "2016-01-05", "NXPRT", "EETLL", []),
        ("2016-01-05", "2016-01-01", "CNGGZ", "EETLL", []),
        (
            "2016-01-03",
            "2016-01-05",
            "CNSGH",
            "north_europe_main",
            [
                ("2016-01-03", 1112),
                ("2016-01-04", 1142),
                ("2016-01-05", 1142),
            ],
        ),
        (
            "2016-01-04",
            "2016-01-06",
            "china_main",
            "RUKGD",
            [
                ("2016-01-04", 1455),
                ("2016-01-05", 1455),
                ("2016-01-06", 1455),
            ],
        ),
        (
            "2016-01-04",
            "2016-01-06",
            "china_main",
            "northern_europe",
            [
                ("2016-01-04", 1456),
                ("2016-01-05", 1455),
                ("2016-01-06", 1442),
            ],
        ),
    ],
    ids=[
        "port_to_port",
        "same_port",
        "nx_port",
        "backwards_range",
        "port_to_region",
        "region_to_port",
        "region_to_region",
    ],
)
def test_get_average_daily_prices(
    dbconn, date_from, date_to, origin, destination, expected_price_data
):
    prices = get_average_daily_prices(
        dbconn,
        date_from=date_from,
        date_to=date_to,
        origin=origin,
        destination=destination,
    )

    price_data = [(str(p.day), p.average_price) for p in prices]
    assert price_data == expected_price_data


def test_price_validation():
    Price(orig_code="EETLL", dest_code="RUKGD", day=date(2021, 1, 1), price=42)

    with pytest.raises(ValidationError):
        Price(orig_code="EETLL", dest_code="EETLL", day=date(2021, 1, 1), price=42)


@pytest.mark.database
def test_ingest_prices(dbconn):
    prices = [
        Price(orig_code="EETLL", dest_code="RUKGD", day=date(2021, 1, 1), price=994),
        Price(orig_code="NOOSL", dest_code="FRLVE", day=date(2021, 1, 4), price=931),
    ]

    dbconn.execute(
        "DELETE FROM prices WHERE "
        "orig_code = %(orig_code)s AND dest_code = %(dest_code)s AND "
        "day = date(%(day)s) AND price = %(price)s",
        [p.dict() for p in prices],
    )

    ingest_prices(dbconn, prices)

    rows = dbconn.execute(
        "SELECT * FROM prices WHERE "
        "orig_code = %(orig_code)s AND dest_code = %(dest_code)s AND "
        "day = date(%(day)s) AND price = %(price)s",
        [p.dict() for p in prices],
    )

    assert rows.rowcount == len(prices)


@pytest.mark.database
def test_ingest_prices_missing_ports(dbconn):
    prices = [
        Price(orig_code="EETLL", dest_code="RUKGD", day=date(2021, 1, 1), price=899),
        Price(orig_code="LOLOL", dest_code="RUKGD", day=date(2021, 1, 1), price=994),
        Price(orig_code="NOOSL", dest_code="WTFPT", day=date(2021, 1, 4), price=931),
    ]

    with pytest.raises(PriceIngestionError) as e:
        ingest_prices(dbconn, prices)

    assert all(p in str(e) for p in ["LOLOL", "WTFPT"])
    assert not any(p in str(e) for p in ["EETLL", "RUGKD", "NOOSL"])

    rows = dbconn.execute(
        "SELECT * FROM prices WHERE "
        "orig_code = %(orig_code)s AND dest_code = %(dest_code)s AND "
        "day = date(%(day)s) AND price = %(price)s",
        [p.dict() for p in prices],
    )

    assert rows.rowcount == 0
