#!/bin/sh

: "${RATES_APP:=rates.api:app}"
: "${RATES_PORT:=8000}"

exec uvicorn \
    --host "0.0.0.0" \
    --port "${RATES_PORT}" \
    "${RATES_APP}" \
    "$@"
