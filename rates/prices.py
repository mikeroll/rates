from datetime import date
from typing import Iterable, List

from pydantic import BaseModel, validator
from sqlalchemy import text

from rates.clients import openexchangerates_client as oxr_client
from rates.clients.openexchangerates import OpenExchangeRatesError
from rates.db import Connection

# This could potentially look tidier if written with SQLAlchemy Core DSL - kept as
# is for demo purposes.
AVERAGE_DAILY_PRICES_QUERY = text(
    """
    -- "Explode" a nested region hierarchy into a flat port <-> region mapping
    WITH RECURSIVE port_regions AS (
        -- Make a port its own region with the same name for simpler WHEREs later
        SELECT code, code AS region FROM ports
        UNION
        SELECT code, parent_slug AS region FROM ports
        UNION
        SELECT code, regions.parent_slug AS region
        FROM regions
            JOIN port_regions ON regions.slug = port_regions.region
        WHERE regions.parent_slug IS NOT NULL
    )
    SELECT
        day,
        CASE WHEN COUNT(price) >= :min_price_count THEN ROUND(AVG(price))
            ELSE NULL
        END AS average_price
    FROM prices p
        JOIN port_regions op ON op.code = p.orig_code
        JOIN port_regions dp ON dp.code = p.dest_code
    WHERE
        op.region = :origin AND
        dp.region = :destination AND
        p.day BETWEEN date(:date_from) AND date(:date_to)
    GROUP BY day
    ORDER BY day;
    """
)


class AverageDailyPrice(BaseModel):
    day: date
    average_price: int


def get_average_daily_prices(
    db: Connection,
    *,
    date_from: date,
    date_to: date,
    origin: str,
    destination: str,
    min_price_count=1,
) -> List[AverageDailyPrice]:
    rows = db.execute(
        AVERAGE_DAILY_PRICES_QUERY,
        origin=origin,
        destination=destination,
        min_price_count=min_price_count,
        date_from=date_from,
        date_to=date_to,
    ).fetchall()

    return [AverageDailyPrice(day=day, average_price=price) for day, price in rows]


class Price(BaseModel):
    orig_code: str
    dest_code: str
    day: date
    price: int
    currency = "USD"

    @validator("dest_code")
    def validate_ports(cls, v, values, **kwargs):
        if "orig_code" in values and v == values["orig_code"]:
            raise ValueError("orig_code and dest_code must not match")

        return v


class CurrencyConversionError(Exception):
    pass


def to_usd_price(price: Price) -> Price:
    try:
        xrate = oxr_client.get_historical_rate(price.day, price.currency)
    except OpenExchangeRatesError as e:
        raise CurrencyConversionError(str(e)) from e

    return price.copy(update={"price": round(price.price / xrate)})


class PriceIngestionError(Exception):
    pass


def ingest_prices(db: Connection, prices: Iterable[Price]) -> None:
    if not prices:
        return

    expected_codes = {c for p in prices for c in (p.orig_code, p.dest_code)}

    with db.begin():
        code_rows = db.execute(
            "SELECT code FROM ports WHERE code IN %s", [(tuple(expected_codes),)]
        ).fetchall()
        codes = {code for (code,) in code_rows}

        if (missing_ports := expected_codes - codes) :
            raise PriceIngestionError(f"unknown port(s): {','.join(missing_ports)}")

        db.execute(
            """INSERT INTO prices VALUES (
                %(orig_code)s, %(dest_code)s, date(%(day)s), %(price)s
            )""",
            [p.dict() for p in prices],
        )
