from functools import lru_cache

from pydantic import BaseSettings


class Settings(BaseSettings):
    class Config:
        env_prefix = "RATES_"

    db_host: str
    db_port: int
    db_name: str
    db_username: str
    db_password: str
    oxr_app_id: str

    @property
    def db_dsn(self) -> str:
        return "postgresql://{username}:{password}@{host}:{port}/{name}".format(
            username=self.db_username,
            password=self.db_password,
            host=self.db_host,
            port=self.db_port,
            name=self.db_name,
        )


@lru_cache
def get_settings() -> Settings:
    return Settings()
