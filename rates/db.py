from typing import Iterator

from sqlalchemy import create_engine
from sqlalchemy.engine import Connection

from rates.settings import get_settings

settings = get_settings()
engine = create_engine(settings.db_dsn)


def get_db_connection() -> Iterator[Connection]:
    with engine.connect() as connection:
        yield connection
