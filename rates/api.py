from datetime import date, timedelta
from typing import List

from fastapi import Depends, FastAPI, HTTPException, status
from pydantic import BaseModel, validator

from rates import __version__
from rates.db import Connection, get_db_connection
from rates.prices import (
    AverageDailyPrice,
    CurrencyConversionError,
    Price,
    PriceIngestionError,
    get_average_daily_prices,
    ingest_prices,
    to_usd_price,
)
from rates.utils import date_range

app = FastAPI(
    title="Rates",
    description="A prototype of a shipping rates tracking app",
    version=__version__,
)


@app.get("/-/healthcheck")
def healthcheck(db: Connection = Depends(get_db_connection)):
    db.execute("SELECT 'OK'")
    return {"api": "ok", "database": "ok"}


@app.get("/rates", response_model=List[AverageDailyPrice])
def rates(
    date_from: date,
    date_to: date,
    origin: str,
    destination: str,
    db: Connection = Depends(get_db_connection),
):
    return get_average_daily_prices(
        db,
        date_from=date_from,
        date_to=date_to,
        origin=origin,
        destination=destination,
    )


# Would really merge this with /rates, exposing `min_price_count` as a query param.
# Keeping it separate as the spec says so :)
@app.get("/rates_null", response_model=List[AverageDailyPrice])
def rates_null(
    date_from: date,
    date_to: date,
    origin: str,
    destination: str,
    min_price_count=3,
    db: Connection = Depends(get_db_connection),
):
    return get_average_daily_prices(
        db,
        date_from=date_from,
        date_to=date_to,
        origin=origin,
        destination=destination,
        min_price_count=min_price_count,
    )


class IncomingPrice(BaseModel):
    # Not validating that "date_from < date_to" - a reversed order will simply result in
    # an empty range. A validator could be added similarly as for
    # "origin_code != destination_code" if this is unwanted.
    date_from: date
    date_to: date
    origin_code: str
    destination_code: str
    price: int
    currency = "USD"

    @validator("destination_code")
    def validate_ports(cls, v, values, **kwargs):
        if "origin_code" in values and v == values["origin_code"]:
            raise ValueError("origin_code and destination_code must not match")

        return v

    def as_daily_prices(self) -> List[Price]:
        days = date_range(self.date_from, self.date_to + timedelta(days=1))

        return [
            Price(
                day=day,
                price=self.price,
                orig_code=self.origin_code,
                dest_code=self.destination_code,
                currency=self.currency,
            )
            for day in days
        ]


@app.post("/prices", status_code=status.HTTP_201_CREATED)
def prices(
    price: IncomingPrice,
    db: Connection = Depends(get_db_connection),
):
    prices = price.as_daily_prices()

    try:
        converted_prices = [to_usd_price(price) for price in prices]
        ingest_prices(db, converted_prices)
    except (CurrencyConversionError, PriceIngestionError) as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"could not ingest prices: {e}",
        )

    return {"created": len(prices)}
