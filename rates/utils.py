from datetime import date, timedelta
from typing import Iterator


def date_range(
    date_from: date, date_to: date, interval=timedelta(days=1)
) -> Iterator[date]:
    prev_date = date_from

    while (next_date := prev_date + interval) <= date_to:
        yield prev_date
        prev_date = next_date
