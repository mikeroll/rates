from rates.clients.openexchangerates import OpenExchangeRatesClient
from rates.settings import Settings

settings = Settings()

openexchangerates_client = OpenExchangeRatesClient(app_id=settings.oxr_app_id)
