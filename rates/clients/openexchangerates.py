from datetime import date
from functools import lru_cache
from typing import Optional

import requests


class OpenExchangeRatesError(Exception):
    pass


class RatesNotAvailable(OpenExchangeRatesError):
    def __init__(self, day: date, base: str, currency: Optional[str] = None):
        self.day = day
        self.base = base
        self.currency = currency

    def __str__(self):
        currencies = f"{self.currency} -> {self.base}" if self.currency else self.base
        return f"no exchange rate available for {currencies} on {self.day}"


class OpenExchangeRatesClient:
    BASE_URL = "https://openexchangerates.org/api"

    def __init__(self, app_id: str):
        self.app_id = app_id
        self.session = requests.Session()

    # TODO: @lru_cache is fine for a prototype, but when/if there are lots of dates to
    # keep track of and memory (or the number of OXR requests after a cold start)
    # becomes a concern, persist these Redis or the database.
    @lru_cache(maxsize=None)
    def get_historical_rates(self, day: date, *, base: str = "USD"):
        response = self.session.get(
            f"{self.BASE_URL}/historical/{day:%Y-%m-%d}.json",
            params={"app_id": self.app_id},
        )
        try:
            response.raise_for_status()
        except requests.HTTPError as e:
            if e.response.status_code == 400:
                raise RatesNotAvailable(day, base)

            # TODO: otherwise, log and/or forward the original exception to
            # Sentry/somewhere we can see - this could be a problem with our app_id etc.

        return response.json()

    def get_historical_rate(
        self, day: date, currency: str, *, base: str = "USD"
    ) -> float:
        if currency == base:
            return 1.0

        rates = self.get_historical_rates(day, base=base)

        try:
            return rates["rates"][currency]
        except KeyError:
            raise RatesNotAvailable(day, base, currency)
