> Imagine you need to receive and update batches of tens of thousands of new prices, conforming to a similar format. Describe, using a couple of paragraphs, how you would design the system to be able to handle those requirements. Which factors do you need to take into consideration?

## The "easy" way
"Tens of thousands" is not that much, it's roughly in the order of 1-10 MB of raw input data, if the format is similar the database schema. Without changing anything architecturally, this could be a `/prices/bulk` POST endpoint with a handler which will: 
- read the prices as request body or a file
- parse and transform the data
- insert into the database`/prices` does.

The process is mostly IO bound, so while completing the request may take seconds, concurrent handling with e.g. asyncio or gevent will help avoid blocking the request queue. It will probably work for a while, until there is talk of hundreds of thousands / millions / etc. With proper tuning we could even handle that, but there is the ever-increasing effort of supporting a type of workload which is better handled by a separate background processing system.

## Background processing

### Recieve the input
The process would be a bit  different, depending on where the data comes from.

If it's uploaded/appears on a remote storage location, e.g. S3:
- Have some way to know when new input is available, e.g. S3 Events or simple cron checks. Have this mechanism create a new processing task upon encountering new input, with the location as an argument.

If it's uploaded by the client via the API as a request body or a file:
- Stream it into a blob such as a file or an S3 object, without doing anything else, return 202 Accepted.
    - Optionally, return a request id or a link, which can be used by the client to check processing status.
- Create a new processing task with the input location as an argument.

### Process and insert in the background
Run a separate service, normally a task queue such as Celery or RQ or Dramatiq, which will:
- Be connected to the same database directly, not having to call the main API service (we can reuse some code bits such as `prices.ingest_prices` though)
- Recieve new "tasks" sent by one of the mechanisms above
- Get the input location from the task details
- Load the input and parse/transform and insert into the database. Probably all in a streaming manner, depending on memory budget and performance.
- Notify about completion - send a notification directly to the original uploader, or just set some `task.result` to be checked later.
